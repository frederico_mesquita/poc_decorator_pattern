package br.com.frederico_mesquita.strutural.control;

import br.com.frederico_mesquita.strutural.service.GuestMemberService;
import br.com.frederico_mesquita.strutural.service.IService;
import br.com.frederico_mesquita.strutural.service.IServiceFactory;
import br.com.frederico_mesquita.strutural.service.MemberType;
import br.com.frederico_mesquita.strutural.service.OneToOneTraining;
import br.com.frederico_mesquita.strutural.service.ProductSupport;
import br.com.frederico_mesquita.strutural.service.RegisteredMemberService;
import br.com.frederico_mesquita.strutural.service.ServiceSubscriptionType;

public class ServiceFactory implements IServiceFactory {
	
	private static ServiceFactory _instance;
	
	private ServiceFactory() {}
	
	public static ServiceFactory getInstance() {
		if(null == _instance) {
			_instance = new ServiceFactory();
		}
		return _instance;
	}

	@Override
	public IService subscribe(MemberType memberType, ServiceSubscriptionType serviceSubscriptionType) {
		IService iService = null;

		switch (memberType) {
		case GUEST:
			iService = getService(serviceSubscriptionType, GuestMemberService.getInstance());
			break;
		case REGISTERED:
			iService = getService(serviceSubscriptionType, RegisteredMemberService.getInstance());
			break;
		default:
			break;
		}

		return iService;
	}
	
	private IService getService(ServiceSubscriptionType serviceSubscriptionType, IService iService) {
		switch (serviceSubscriptionType) {
		case ONE2ONE_TRAINNING:
			iService = new OneToOneTraining(iService);
			break;
		case PRODUCT_SUPPORT:
			iService = new ProductSupport(iService);
			break;
		default:
			break;
		}
		return iService;
	}

	@Override
	public IService subscribe(MemberType memberType, ServiceSubscriptionType serviceSubscriptionType, IService iService) {
		switch (memberType) {
		case GUEST:
			iService = getService(serviceSubscriptionType, iService);
			break;
		case REGISTERED:
			iService = getService(serviceSubscriptionType, iService);
			break;
		default:
			break;
		}

		return iService;
	}

}
