package br.com.frederico_mesquita.strutural.entity;

import java.time.Instant;
import java.util.UUID;

public class LifetimeMember extends Membership {

	public void registerMember(String param) {
		this.setUserId(UUID.randomUUID().toString());
		this.setUserName(param);
		this.setStartDate(Instant.now());
		this.setEndDate(60);
		this.setDiscountPercentage(new Float(90));
		this.setSubscriptionType(SubscriptionType.LIFETIME);
	}

}
