package br.com.frederico_mesquita.strutural.entity;

import br.com.frederico_mesquita.strutural.service.IService;
import br.com.frederico_mesquita.strutural.service.MemberType;

public interface IMembership {
	
	void registerMember(String param);
	
	void showMember();

	IService getiService();

	void setiService(IService iService);

	MemberType getMemberType();

	void setMemberType(MemberType memberType);
}
