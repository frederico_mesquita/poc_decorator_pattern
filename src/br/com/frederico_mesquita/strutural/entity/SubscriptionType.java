package br.com.frederico_mesquita.strutural.entity;

public enum SubscriptionType {
	ANNUAL_MEMBER, TEMPORARY_MEMBER, LIFETIME;
}
