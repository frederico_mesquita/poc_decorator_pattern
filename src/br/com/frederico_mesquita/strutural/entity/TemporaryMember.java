package br.com.frederico_mesquita.strutural.entity;

import java.time.Instant;
import java.util.UUID;

public class TemporaryMember extends Membership {

	@Override
	public void registerMember(String param) {
		this.setUserId(UUID.randomUUID().toString());
		this.setUserName(param);
		this.setStartDate(Instant.now());
		this.setEndDate(0);
		this.setDiscountPercentage(new Float(10));
		this.setSubscriptionType(SubscriptionType.TEMPORARY_MEMBER);
	}

}
