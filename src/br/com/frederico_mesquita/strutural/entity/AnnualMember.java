package br.com.frederico_mesquita.strutural.entity;

import java.time.Instant;
import java.util.UUID;

public class AnnualMember extends Membership {

	@Override
	public void registerMember(String param) {
		this.setUserId(UUID.randomUUID().toString());
		this.setUserName(param);
		this.setStartDate(Instant.now());
		this.setEndDate(1);
		this.setDiscountPercentage(new Float(30));
		this.setSubscriptionType(SubscriptionType.ANNUAL_MEMBER);
	}

}
