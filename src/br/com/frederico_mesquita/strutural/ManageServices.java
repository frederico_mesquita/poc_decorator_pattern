package br.com.frederico_mesquita.strutural;

import java.util.ArrayList;
import java.util.List;

import br.com.frederico_mesquita.strutural.control.MemberFactory;
import br.com.frederico_mesquita.strutural.control.ServiceFactory;
import br.com.frederico_mesquita.strutural.entity.IMembership;
import br.com.frederico_mesquita.strutural.entity.SubscriptionType;
import br.com.frederico_mesquita.strutural.service.MemberType;
import br.com.frederico_mesquita.strutural.service.ServiceSubscriptionType;

public class ManageServices {
	public static void main(String[] args) {
		List<IMembership> lstMember = new ArrayList<>();
		
		IMembership teddy = MemberFactory.getMemberFactory().subscribe(SubscriptionType.ANNUAL_MEMBER, "Teddy");
		IMembership robert = MemberFactory.getMemberFactory().subscribe(SubscriptionType.TEMPORARY_MEMBER, "Robert");
		IMembership suzan = MemberFactory.getMemberFactory().subscribe(SubscriptionType.LIFETIME, "Suzan");
		
		lstMember.add(teddy);
		lstMember.add(robert);
		lstMember.add(suzan);
		
		//--xx--xx--xx--xx--xx--xx--xx--xx--xx--xx--xx--xx--xx--xx--xx--xx--xx--xx--xx--xx--xx--xx--xx--xx--xx--xx
		
		teddy.setMemberType(MemberType.REGISTERED);
		robert.setMemberType(MemberType.GUEST);
		suzan.setMemberType(MemberType.REGISTERED);
		
		teddy.setiService(ServiceFactory.getInstance().subscribe(teddy.getMemberType(), ServiceSubscriptionType.ONE2ONE_TRAINNING));
 
		robert.setiService(ServiceFactory.getInstance().subscribe(robert.getMemberType(), ServiceSubscriptionType.ONE2ONE_TRAINNING));
		robert.setiService(ServiceFactory.getInstance().subscribe(robert.getMemberType(), ServiceSubscriptionType.PRODUCT_SUPPORT, robert.getiService()));

		suzan.setiService(ServiceFactory.getInstance().subscribe(suzan.getMemberType(), ServiceSubscriptionType.PRODUCT_SUPPORT));
		
		for(IMembership member : lstMember) {
			member.showMember();
			System.out.println(member.getiService().toString() + "\n\n");
		}
		
	}
}
