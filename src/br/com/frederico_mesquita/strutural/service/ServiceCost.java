package br.com.frederico_mesquita.strutural.service;

public enum ServiceCost {
	REGISTERED_MEMBER_SERVICE_COST(10.0),
	GUEST_MEMBER_SERVICE_COST(18.0),
	ONE_TO_ONE_TRAINING_SERVICE_COST(51.0),
	PRODUCT_SUPPORT_SERVICE_COST(87.0);
	
	private double cost;

	ServiceCost(double _cost) {
		 this.cost = _cost;
	}
	
	public double getCost() {
		return this.cost;
	}
}
