package br.com.frederico_mesquita.strutural.service;

public enum MemberType {
	REGISTERED, GUEST;
}
