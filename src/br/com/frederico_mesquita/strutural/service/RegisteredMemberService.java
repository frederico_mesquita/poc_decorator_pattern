package br.com.frederico_mesquita.strutural.service;

public class RegisteredMemberService implements IService {
	
	private static RegisteredMemberService _instance;
	
	private RegisteredMemberService() {}
	
	public static RegisteredMemberService getInstance() {
		if(null == _instance) {
			_instance = new RegisteredMemberService();
		}
		return _instance;
	}
	
	@Override
	public String getDescription() {
		return ServiceMessage.REGISTERED_MEMBER_SERVICE_DESCRIPTION.getMessage();
	}

	@Override
	public double getCost() {
		return ServiceCost.REGISTERED_MEMBER_SERVICE_COST.getCost();
	}
	
}
