package br.com.frederico_mesquita.strutural.service;

public interface IService {
	public String getDescription();
	public double getCost();
}
