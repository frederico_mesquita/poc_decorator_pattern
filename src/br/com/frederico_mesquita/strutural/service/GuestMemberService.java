package br.com.frederico_mesquita.strutural.service;

public class GuestMemberService implements IService {
	
	private static GuestMemberService _instance;
	
	private GuestMemberService() {}
	
	public static GuestMemberService getInstance() {
		if(null == _instance) {
			_instance = new GuestMemberService();
		}
		return _instance;
	}
	
	@Override
	public String getDescription() {
		return ServiceMessage.GUEST_MEMBER_SERVICE_DESCRIPTION.getMessage();
	}

	@Override
	public double getCost() {
		return ServiceCost.GUEST_MEMBER_SERVICE_COST.getCost();
	}

}
