package br.com.frederico_mesquita.strutural.service;

import javax.json.Json;

public class ProductSupport extends Service {

	public ProductSupport(IService _iService) {
		super(_iService);
	}

	@Override
	public String getDescription() {
		return super._iService.getDescription() + ServiceMessage.PRODUCT_SUPPORT_SERVICE_DESCRIPTION.getMessage();
	}

	@Override
	public double getCost() {
		return super._iService.getCost() + ServiceCost.PRODUCT_SUPPORT_SERVICE_COST.getCost();
	}
	
	public String toString() {
		return Json.createObjectBuilder()
			.add("consumedService", this.getDescription())
			.add("cost", this.getCost()).build().toString();
	}

	public void showService() {
		System.out.println(this.toString());
	}

}
