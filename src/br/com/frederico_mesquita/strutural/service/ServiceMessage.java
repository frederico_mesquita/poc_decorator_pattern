package br.com.frederico_mesquita.strutural.service;

public enum ServiceMessage {
	REGISTERED_MEMBER_SERVICE_DESCRIPTION("Addicional service for registered member."),
	GUEST_MEMBER_SERVICE_DESCRIPTION("Addicional service for guest member."),
	ONE_TO_ONE_TRAINNING_SERVICE_DESCRIPTION("\n\tOne2One Trainning service consumed."),
	PRODUCT_SUPPORT_SERVICE_DESCRIPTION("\n\tProduct Support service consumed.");
	
	private String msg;

	ServiceMessage(String _msg) {
		 this.msg = _msg;
	}
	
	public String getMessage() {
		return this.msg;
	}
}
